package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

import u03.Lists.List
import u02.Optionals

class ListTest {

  import Lists.List._
  import Optionals.Option._

  val lst = Cons(10, Cons(20, Cons(30, Nil())))

  @Test def sumTestTest(): Unit = {
    assertEquals(60, sum(lst));
  }

  @Test def appendTest(): Unit = {
    val list = Cons(5, Cons(10, Cons(20, Cons(30,  Nil()))))
    assertEquals(list , List.append(Cons(5, Nil()), lst))
  }

  @Test def filterTest(): Unit = {
    assertEquals(Cons(20, Cons(30, Nil())), List.filter[Int](lst)(_ >=20))
  }

  @Test def dropTest(): Unit ={
    assertEquals(Cons(20, Cons(30, Nil())), List.drop(lst, 1))
    assertEquals(Cons(30, Nil()), List.drop(lst, 2))
    assertEquals(Nil(), List.drop(lst, 5))
  }

  @Test def flatMapTest(): Unit = {
    assertEquals(Cons (11 , Cons (21 , Cons (31 , Nil ()))), List.flatMap(lst)(v=> Cons(v+1, Nil())))
    assertEquals(Cons(11 , Cons(12 , Cons(21 , Cons(22 , Cons(31 , Cons(32 , Nil())))))), List.flatMap(lst)(v => Cons ( v +1 , Cons (v +2 , Nil () ))))
  }

  @Test def mapTest(): Unit ={
    assertEquals(Cons(13, Cons(23, Cons(33, Nil()))), List.mapWithFlatMap(lst)(_+3))
  }

  @Test def filterNewTest(): Unit = {
    assertEquals(Cons(20, Cons(30, Nil())), List.filterWithFlatMap[Int](lst)(_ >=20))
    assertEquals(Cons(10, Cons(20, Cons(30, Nil()))), List.filterWithFlatMap[Int](lst)(_ >=10))
  }

  @Test def maxTest(): Unit ={
    assertEquals(Some(30), List.max(lst))
    assertEquals(None() , List.max(Nil()))
  }

  @Test def foldedListTest(): Unit ={
    val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    assertEquals(-16, List.foldLeft(lst)(0)(_-_))
    assertEquals(16, List.foldLeft(lst)(0)(_+_))
    assertEquals(Cons(5, Cons(1, Cons(7, Cons(3, Nil())))), List.reverse(lst))

    assertEquals(-8, List.foldRight(lst)(0)(_-_))
  }

}

