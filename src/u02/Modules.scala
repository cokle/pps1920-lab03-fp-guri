package u02

import u03.Lists._
import u03.Lists.List._

object Modules extends App {

  // An ADT: type + module
  sealed trait Person
  object Person {

    case class Student(name: String, year: Int) extends Person

    case class Teacher(name: String, course: String) extends Person

    def name(p: Person): String = p match {
      case Student(n, _) => n
      case Teacher(n, _) => n
    }

    def getCoursesFrom(l: List[Person]): List[String] = l match {
      case Cons(head, tail) => head match {
        case Teacher(_, course) => flatMap(Cons(course, Nil()))(c=>Cons[String](course, getCoursesFrom(tail)))
        case Student(_, _) =>  getCoursesFrom(tail)
      }
      case _ => Nil()
    }
  }
}
