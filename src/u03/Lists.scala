package u03

import u02.Optionals

object Lists {

  // A generic linkedlist
  sealed trait List[E]
  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if pred(h) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }

    @annotation.tailrec
    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(_, tail) if n>0 => drop(tail, n-1)
      case _ => l
    }

    def flatMap[A,B](l: List[A])(f: A=>List[B]): List[B] = l match {
      case Cons(head, tail) => append(f(head), flatMap(tail)(f))
      case _ => Nil()
    }

    def mapWithFlatMap[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(_, _) => flatMap(l)(h => Cons(mapper(h), Nil()))
      case _ => Nil()
    }

    @annotation.tailrec
    def filterWithFlatMap[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(head, _) if pred(head) => flatMap(l1)(head => Cons(head, Nil()))
      case Cons(_, tail) => filterWithFlatMap(tail)(pred)
      case _ => Nil()
    }

    import u02.Optionals._
    def max(l: List[Int]): Option[Int] =   l match {
      case Nil() => Option.None()
      case Cons(head, _) =>

        @annotation.tailrec
        def compareMaxElem(list: List[Int], max: Int): Option[Int] = list match {
          case Cons(h,t)  if h>=max => compareMaxElem(t, h)
          case Cons(_,t) => compareMaxElem(t, max)
          case _ => Option.Some[Int](max)
        }
        compareMaxElem(l, head)
    }

    @annotation.tailrec
    def foldLeft[A, B](list: List[A])(acc: B)(f: (B,A)=>B): B = list match {
      case Cons(head, tail) =>  println(tail);println(acc+ " - " + head+" = "+ f(acc,head));foldLeft(tail)(f(acc,head))(f)
      case _ => acc
    }

     /*def foldRight[A, B](list: List[A])(acc: B)(f: (B,A)=>B): B = reverse(list) match {
        case Cons(head, tail) => foldLeft(tail)(f(acc, head))(f)
    } */

    def foldRight[A, B](l: List[A])(init: B)(pred: (A, B) => B): B = l match {
      case Cons(h, t) => pred(h, foldRight(t)(init)(pred))
      case _ => init
    }

    def reverse[A](list: List[A]): List[A] = {
      @annotation.tailrec
      def reverseList(result: List[A], list: List[A]): List[A] = list match {
        case Nil() => result
        case Cons(head, tail) => reverseList(Cons(head, result), tail)
      }
      reverseList(Nil(), list)
    }

  }
}


