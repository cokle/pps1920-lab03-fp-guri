package soluzioni

import u02.Modules.Person
import u02.Modules.Person._
import u03.Lists.List.{Cons, _}
import u03.Lists.List
import u03.Streams.Stream
import u03.Streams.Stream._
import u02.Optionals.Option

class SoluzioneL3 {

  object Esercizio1 {
    // 1.a
    @annotation.tailrec
    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(_, tail) if n>0 => drop(tail, n-1)
      case _ => l
    }

    // 1.b
    def flatMap[A,B](l: List[A])(f: A=>List[B]): List[B] = l match {
      case Cons(head, tail) => append(f(head), flatMap(tail)(f))
      case _ => Nil()
    }

    // 1.c
    def mapWithFlatMap[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(_, _) => flatMap(l)(h => Cons(mapper(h), Nil()))
      case _ => Nil()
    }

    // 1.d
    @annotation.tailrec
    def filterWithFlatMap[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(head, _) if pred(head) => flatMap(l1)(head => Cons(head, Nil()))
      case Cons(_, tail) => filterWithFlatMap(tail)(pred)
      case _ => Nil()
    }

  }


  object Esercizio2 {
    def max(l: List[Int]): Option[Int] =   l match {
      case Nil() => Option.None()
      case Cons(head, _) =>

        @annotation.tailrec
        def compareMaxElem(list: List[Int], max: Int): Option[Int] = list match {
          case Cons(h,t)  if h>=max => compareMaxElem(t, h)
          case Cons(_,t) => compareMaxElem(t, max)
          case _ => Option.Some[Int](max)
        }
        compareMaxElem(l, head)
    }
  }


  object Esercizio3 {
    def getCoursesFrom(l: List[Person]): List[String] = l match {
      case Cons(head, tail) => head match {
        case Teacher(_, course) => flatMap(Cons(course, Nil()))(_ =>Cons(course, getCoursesFrom(tail)))
        case Student(_, _) =>  getCoursesFrom(tail)
      }
      case _ => Nil()
    }
  }


  object Esercizio4 {
    // 4.1
    @annotation.tailrec
    def foldLeft[A, B](list: List[A])(acc: B)(f: (B,A)=>B): B = list match {
      case Cons(head, tail) => foldLeft(tail)(f(acc,head))(f)
      case _ => acc
    }

    // 4.2
    def foldRight[A, B](l: List[A])(init: B)(pred: (A, B) => B): B = l match {
      case Cons(h, t) => pred(h, foldRight(t)(init)(pred))
      case _ => init
    }

    // Trying to implement foldRight following the slide's hint. NOT WORKING
    def foldRight2[A, B](list: List[A])(acc: B)(f: (B,A)=>B): B = reverse(list) match {
        case Cons(head, tail) => foldLeft(tail)(f(acc, head))(f)
        case _ => acc
    }

    def reverse[A](list: List[A]): List[A] = {
      @annotation.tailrec
      def reverseList(result: List[A], list: List[A]): List[A] = list match {
        case Nil() => result
        case Cons(head, tail) => reverseList(Cons(head, result), tail)
      }
      reverseList(Nil(), list)
    }
  }


   object Esercizio5 {
    @annotation.tailrec
    def drop[A](stream: Stream[A])(n: Int): Stream[A] = stream match {
      case Cons(_, tail) if n>0 => drop(tail())(n-1)
      case _ => stream
    }
  }


  object Esercizio6 {
    def constant[A](k: A): Stream[A] = cons(k, constant(k))
  }


  object Esercizio7 {
    val fibs : Stream [Int ] = {
      def tail(h: Int, n: Int): Stream[Int] = cons(h, tail( h+n,h))
      tail(0, 1)
    }
  }


}
